% save all images in a column

extension = '.bmp'; %'.jpg';
method = 'median128';
width = 34;
height = 18;
profile = 1; % 3 color, 1 grays
randomize = true;
size_imgs = [num2str(width) 'x' num2str(height)];

% grays
folder_imgs = ['C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\cfd-boy-neutral-allraces\eyes-without-mask\same-size\homogenized_' method '\' size_imgs '\'];
% color
% folder_imgs = ['C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\cfd-boy-neutral-allraces\eyes-without-mask\same-size\' size_imgs '\'];
folder_results = fullfile(folder_imgs, 'allimages-in-a-column\');
if(~exist(folder_results, 'dir')), mkdir(folder_results); end;



% list all 'extension' files in 'folder_imgs'
img_filenames = dir([folder_imgs 'CFD' '*' extension]); % 'CFD' to avoid masks_
n_img_filenames = numel(img_filenames);

hw = waitbar(0, 'Joining images... 0%');
t=tic;

img_col = uint8(zeros(height*n_img_filenames, width, profile));

if(randomize)
    rn = randperm(n_img_filenames);
    rnname = 'randomized';
else
    rn = 1:n_img_filenames;
    rnname = 'ordered';
end

for nf=1:n_img_filenames

    waitbar(nf/n_img_filenames, hw, sprintf('Joining images... %.2f%%', nf/n_img_filenames*100));
    
    % % Image filename
    filename = img_filenames(nf).name;
    
    img = imread(fullfile(folder_imgs, filename));
    
    ni = rn(nf);
    img_col((ni-1)*height+1:ni*height, :, :) = img;
    
end
close(hw)

imwrite(img_col, fullfile(folder_results, ['allimages_' size_imgs '_' method '_' rnname '.bmp']));
toc(t);