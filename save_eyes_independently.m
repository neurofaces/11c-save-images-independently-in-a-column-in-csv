% run_face_parameterization

pri = priority('h');

folder_imgs = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\cfd-boy-neutral-allraces\';
folder_masks = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\cfd-boy-neutral-allraces\masks-corrected\mats\';
folder_eyebrows = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\cfd-boy-neutral-allraces\extracted-eb-corrected\mats\';
folder_landmarks = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\cfd-boy-neutral-allraces\landmarks-corrected\';
folder_results = fullfile(folder_imgs, 'eyes-without-mask\');
if(~exist(folder_results, 'dir')), mkdir(folder_results); end;

extension = '.jpg';

% list all 'extension' files in 'folder_imgs'
img_filenames = dir([folder_imgs '*' extension]);
n_img_filenames = numel(img_filenames);


h = waitbar(0, 'Extracting eyes... 0%');
t=tic;
% start the loop for every image in the list

% to check parameterization results
for nf=1:n_img_filenames
    
    waitbar(nf/n_img_filenames, h, sprintf('Extracting eyes... %.2f%%', nf/n_img_filenames*100));
    
    % % Image filename
    filename = strrep(img_filenames(nf).name, extension, '');
    
    % % Load image
    img = imread(fullfile(folder_imgs, [filename extension]));
    
    % % Load landmarks
    landmarks = load(fullfile(folder_landmarks, [filename '.lm']));
    
    % % Load masks
    masks = load(fullfile(folder_masks, [filename '_masks.mat']));
    
    % % Left eye
    extremale = regionprops(masks.mask_LE, 'Extrema'); extremale = extremale.Extrema;
    tople = round(min(extremale(1:2,2)));
    bottomle = round(max(extremale(5:6,2)));
    leftle = round(max(extremale(3:4,1)));
    rightle = round(min(extremale(7:8,1)));
    eye_left = img(tople:bottomle,rightle:leftle,:); % .* uint8(repmat(masks.mask_LE(top:bottom,right:left), 1, 1, 3));
    
    % % Right eye
    extremare = regionprops(masks.mask_RE, 'Extrema'); extremare = extremare.Extrema;
    topre = round(min(extremare(1:2,2)));
    bottomre = round(max(extremare(5:6,2)));
    leftre = round(max(extremare(3:4,1)));
    rightre = round(min(extremare(7:8,1)));
    eye_right = fliplr(img(topre:bottomre,rightre:leftre,:)); % .* uint8(repmat(masks.mask_RE(top:bottom,right:left), 1, 1, 3)));
    
    imwrite(eye_left, fullfile(folder_results, strcat(filename, '_L.jpg')));
    imwrite(eye_right, fullfile(folder_results, strcat(filename, '_R.jpg')));
    imwrite(fliplr(masks.mask_LE(tople:bottomle,rightle:leftle)), fullfile(folder_results, strcat('mask_', filename, '_L.jpg')));
    imwrite(masks.mask_RE(topre:bottomre,rightre:leftre), fullfile(folder_results, strcat('mask_', filename, '_R.jpg')));
    
end
disp('Execution ended.');
toc(t)

close(h);

priority(pri);