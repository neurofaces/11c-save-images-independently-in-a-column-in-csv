% save images in text file

folder_imgs = 'C:\Users\ffuentes.I3BH\Documents\DOCTORADO\01_DATA\_common_dbs\cfd-boy-neutral-allraces\eyes-without-mask\same-size\homogenized_median128\34x18\';
extension = '.bmp'; %'.jpg';

% list all 'extension' files in 'folder_imgs'
img_filenames = dir([folder_imgs 'CFD' '*' extension]); % 'CFD' to avoid masks_
n_img_filenames = numel(img_filenames);

hw = waitbar(0, 'Creating file... 0%');
t=tic;

np = 34*18;

fid = fopen(fullfile(folder_imgs, 'images.txt'),'w');
fprintf(fid,'Image_name,');
for i=1:np-1
    fprintf(fid,'%d,',i);
end
fprintf(fid,'%d\n', i+1);

for nf=1:n_img_filenames

    waitbar(nf/n_img_filenames, hw, sprintf('Creating file... %.2f%%', nf/n_img_filenames*100));
    
    % % Image filename
    filename = img_filenames(nf).name;
    
    img = imread(fullfile(folder_imgs, filename));

    fprintf(fid,'%s,', strrep(filename, extension, ''));
    for i=1:np-1
        fprintf(fid,'%d,',img(i));
    end
    fprintf(fid,'%d\n', img(end));
    
end
fclose(fid);
close(hw)